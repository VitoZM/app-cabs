import { StyleSheet, Text, View } from "react-native";
import HomeScreen from "../screens/HomeScreen";
import ExplorerScreen from "../screens/ExplorerScreen";
import CustomDrawer from "../components/CustomDrawer";
import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Ionicons from "react-native-vector-icons/Ionicons";
import { colors } from "../components/colors";

const Drawer = createDrawerNavigator();

const AppStack = () => {
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
        headerShown: false,
        drawerActiveBackgroundColor: colors.primary,
        drawerActiveTintColor: colors.white,
        drawerInactiveTintColor: colors.tertiary,
        drawerLabelStyle: {
          marginLeft: -25,
          fontFamily: 'Roboto',
          fontSize: 15,
        }
      }}
    >
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          drawerIcon: ({ color }) => {
            return <Ionicons name="home-outline" size={22} color={color} />;
          },
        }}
      />

      <Drawer.Screen
        name="Explorer"
        component={ExplorerScreen}
        options={{
          drawerIcon: ({ color }) => {
            return <Ionicons name="home-outline" size={22} color={color} />;
          },
        }}
      />
    </Drawer.Navigator>
  );
};

export default AppStack;

const styles = StyleSheet.create({});
