import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { auth } from '../firebase'
import AuthStack from '../navigation/AuthStack';
import AppStack from '../navigation/AppStack';

const AppNavContainer = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    useEffect( () => {
        const unsuscribe = auth.onAuthStateChanged((user) => {
            if (user) {
                setIsLoggedIn(true)
              console.log("user")
            }else{
                setIsLoggedIn(false)
                console.log("not")
            }
          });
      
          return unsuscribe;
    })

  return (
    <NavigationContainer>
        { isLoggedIn ? <AppStack /> : <AuthStack /> }
    </NavigationContainer>
  );
};

export default AppNavContainer;
