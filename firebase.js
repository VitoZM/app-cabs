// Import the functions you need from the SDKs you need
import * as firebase from "firebase";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBIhQlKUiSJ8iEVwv3vQsvKf0k4iz-xNd8",
  authDomain: "cabs-app-3cdc5.firebaseapp.com",
  projectId: "cabs-app-3cdc5",
  storageBucket: "cabs-app-3cdc5.appspot.com",
  messagingSenderId: "487524857412",
  appId: "1:487524857412:web:44ceaeb448ee39c55dae11"
};

// Initialize Firebase
let app;
if(firebase.apps.length === 0){
    app = firebase.initializeApp(firebaseConfig);
}else{
    app = firebase.app();
}

const auth = firebase.auth();
const database = firebase.database();
export {auth, database};