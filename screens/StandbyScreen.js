import { useNavigation } from "@react-navigation/core";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Dimensions,
} from "react-native";
import React, { useState, useEffect } from "react";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { windowWidth, windowHeight } from "../utils/Dimensions";
import { colors } from "../components/colors";

const StandbyScreen = () => {
  const navigation = useNavigation();
  const redirect = () => {
    navigation.navigate("Home");
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: colors.white,
      }}
    >
      <View style={{ marginTop: 100 }}>
        <Text
          style={{
            fontSize: 30,
            fontWeight: "bold",
            color: "#20315f",
            fontFamily: "Roboto",
          }}
        >
          TU TAXI
        </Text>
      </View>

      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <ImageBackground
          source={require("../assets/images/taxi.png")}
          style={{
            height: windowHeight - 450,
            width: windowWidth - 40,
          }}
        />
      </View>

      <TouchableOpacity
        style={{
          backgroundColor: "#AD40AF",
          padding: 20,
          width: "90%",
          borderRadius: 5,
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 50,
        }}
        onPress={redirect}
      >
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 18,
            color: "#fff",
            fontFamily: "Roboto",
          }}
        >
          ¡ Empezemos !
        </Text>
        <MaterialIcons name="arrow-forward-ios" size={22} color="#fff" />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default StandbyScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "#4D4A95",
    width: "60%",
    padding: 15,
    borderRadius: 10,
    alignItems: "center",
    marginTop: 40,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
});
