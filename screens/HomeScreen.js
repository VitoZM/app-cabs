import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  SafeAreaView,
  StatusBar,
  Modal,
  Animated,
} from "react-native";
import React, { useState, useEffect } from "react";
import { database } from "../firebase";
import { useNavigation } from "@react-navigation/core";

import * as Location from "expo-location";
import TimerFixer from "../components/TimerFixer";
import CustomSwitch from "../components/CustomSwitch";
import ListItem from "../components/ListItem";
import RideSection from "../components/Rides/RideSection";

import BottomSheet from "../components/BottomSheet";
import { colors } from "../components/colors";
import CircleButton from "../components/buttons/CircleButton";
import ExplorerScreen from "../screens/ExplorerScreen";

import ModalPopUp from "../components/ModalPopUp";
import ClientCard from "../components/ClientCard";

TimerFixer.fixer();

const HomeScreen = () => {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [isActive, setIsActive] = useState(false);
  const [viewTab, setViewTab] = useState(1);
  const [visible, setVisible] = React.useState(true); //convertir el modal en component con parámetros como mensaje

  const onSelectSwitch = (value) => {
    setViewTab(value);
  };

  function toggle() {
    setIsActive(!isActive);
  }

  useEffect(() => {
    setData();
  }, []);

  const timeOut = setTimeout(() => {
    if (isActive) {
      _getLocation();
      getData();
    } else {
      clearInterval(timeOut);
    }
  }, 5555);

  const navigation = useNavigation();

  _getLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      setErrorMsg("El permiso de acceder a la ubicación fué denegado");
      return;
    }
    let location = await Location.getCurrentPositionAsync({});
    setLocation(location);
    console.log(location.coords.latitude, location.coords.longitude);
  };

  getData = () => {
    database.ref("cart-2/").on("value", (snapshot) => {
      //console.log(snapshot.val())
    });
  };

  setData = () => {
    database.ref("cart-2/").push().set({
      item5: "escoba",
    });
  };

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        navigation.replace("Login");
      })
      .catch((error) => alert(error.message));
  };
  //debemos seguir viendo como meter el mapa para el fondo
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.night }}>
      <ModalPopUp visible={visible}>
        <View style={{ alignItems: "center" }}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => setVisible(false)}>
              <Image
                source={require("../assets/images/x.png")}
                style={{ height: 30, width: 30 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ alignItems: "center" }}>
          <Image
            source={require("../assets/images/success.png")}
            style={{ height: 150, width: 150, marginVertical: 10 }}
          />
        </View>

        <Text style={{ marginVertical: 30, fontSize: 20, textAlign: "center" }}>
          Congratulations registration was successful
        </Text>
      </ModalPopUp>
      <StatusBar barStyle="light-content" backgroundColor={colors.night} />
      <ClientCard />
      <ScrollView style={{ padding: 20 }}>
        { /*<View
          style={{
            marginTop: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 20,
          }}
        >
          <View
            style={{
              borderRadius: 10,
              borderColor: colors.white,
              borderWidth: 1,
              padding: 10,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontFamily: "Roboto",
                color: colors.white,
              }}
            >
              5 Carreras Realizadas
            </Text>
          </View>
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <ImageBackground
              source={require("../assets/images/rick-and-morty.jpg")}
              style={{ width: 35, height: 35 }}
              imageStyle={{ borderRadius: 25 }}
            />
          </TouchableOpacity>
        </View> */ }
        
        <ExplorerScreen />
        {/* <View style={{marginVertical:20}}>
                <CustomSwitch 
                    selectionMode={1}
                    option1="FUERA DE LÍNEA"
                    option2="TRABAJANDO"
                    onSelectSwitch={onSelectSwitch}
                />
            </View>
            {viewTab == 1 && <ListItem />}
            {viewTab == 2 && <Text>Tab Dos</Text>} */}
      </ScrollView>
      <CircleButton />

      <RideSection />
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: colors.night,
    width: "60%",
    padding: 15,
    borderRadius: 10,
    alignItems: "center",
    marginTop: 40,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
  modalBackGround: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    width: "80%",
    backgroundColor: "white",
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
  },
  header: {
    width: "100%",
    height: 40,
    alignItems: "flex-end",
    justifyContent: "center",
  },
});
