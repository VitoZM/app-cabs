import * as React from 'react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { mapNightStyle } from '../components/MapStyles';
import { colors } from '../components/colors';

const ExplorerScreen = () => {
    
  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map} 
        customMapStyle={mapNightStyle}
        region={{
            latitude: -19.024890,
            longitude: -65.280249,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        }}
      >
          <Marker 
              coordinate={{
                latitude: -19.024890,
                longitude: -65.280249,
              }}
              image={require('../assets/images/car-marker_opt.png')}
              title="Mi Ubicación"
              description='La posición en la que me encuentro'
          />
          
      </MapView>

    </View>
  );
}

export default ExplorerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width - 20,
    height: Dimensions.get('window').height - 50,
  },
});