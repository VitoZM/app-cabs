import { SafeAreaView, TouchableOpacity, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colors } from '../components/colors'

const OnBoardingScreen = ({navigation}) => {
  return (
    <SafeAreaView
        style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
        }}
    >
      <View style={{marginTop: 20}}>
        <Text
            style={{
                fontFamily: 'Roboto',
                fontWeight: 'bold',
                fontSize: 30,
                color: colors.primary,
            }}
        >
            TAXIDRIVE
        </Text>
      </View>
    </SafeAreaView>
  )
}

export default OnBoardingScreen

const styles = StyleSheet.create({})