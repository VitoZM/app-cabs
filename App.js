import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { auth } from './firebase';
import AppNavContainer from './navigation';

export default function App() {
  const [isLogged, setIsLogged] = useState(false);
  useEffect(() => {
    const unsuscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        setIsLogged(true)
      }
    });

    return unsuscribe;
  }, []);

  return (
    /*<SafeAreaProvider>
      <SplashScreen></SplashScreen>
    </SafeAreaProvider>//*/
    <AppNavContainer /> //*/
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
