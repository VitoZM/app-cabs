import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { Dimensions } from 'react-native';
import { colors } from './colors';
import Ionicons from "react-native-vector-icons/Ionicons"

const greenLocation = require('../assets/images/green-location.png')
const blueLocation = require('../assets/images/blue-location.png')

export default function ListItem(props) {

  const image = props.image == "1" ? greenLocation : blueLocation
  const color = props.image == "1" ? 'green' : '#0f5bff'
  return (
    <View style={{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor: colors.tertiary,
        borderRadius: 8,
        borderBottomWidth: 1,
        borderBottomColor: colors.gray,
    }}>
        <View
            style={{
                flexDirection:'row',
                alignItems:'center',
                flex:1,
            }}
        >
            <Image
                source={ image }
                style={{
                    width:50,
                    height:50,
                    borderRadius:10,
                    marginRight:8,
                    margin: 3,
                }}
            />
            <View style={{
                width: Dimensions.get('window').width - 120
            }}>
                <Text
                    style={{
                        color:colors.white,
                        fontFamily: 'Roboto',
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: color
                    }}
                >
                    {props.which} <Ionicons name="ios-location-outline" color={color} />
                </Text>
                <Text
                    numberOfLines={1}
                    style={{
                        color:colors.white,
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        fontWeight: 'bold',
                    }}
                >
                    {props.address}
                </Text>
                <Text
                    numberOfLines={1}
                    style={{
                        color:colors.white,
                        fontFamily: 'Roboto',
                        fontSize: 5,
                    }}
                >
                    
                </Text>
                <Text
                    numberOfLines={3}
                    style={{
                        color:colors.white,
                        fontFamily: 'Roboto',
                        fontSize: 12,
                    }}
                >
                    asdfasdfasdfasdf dfsaasdfasdf asdf sdf asdf asdf
                </Text>
            </View>
        </View>
        
    </View>
  )
}

const styles = StyleSheet.create({})