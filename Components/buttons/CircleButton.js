import { Text, View, TouchableOpacity } from "react-native";
import React from "react";
import { colors } from "../colors";

const CircleButton = () => {
  return (
    <View
      style={{
        height: 130,
        justifyContent: "center",
      }}
    >
      <TouchableOpacity
        style={{
          flex: 1,
          marginBottom: 70,
          borderRadius: 20,
          backgroundColor: colors.secondary,
          width: 200,
          alignItems: "center",
          flexDirection: "row",
          justifyContent: "center",
          alignSelf: "center",
        }}
        onPress={() => {}}
      >
        <View
          style={{
            borderColor: 'white',
            borderWidth: 1,
            borderRadius: 14,
            width: "90%",
            height: "70%",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              color: colors.white,
              fontFamily: "Roboto",
              fontWeight: "bold",
              fontSize: 18,
              alignSelf: "center",
              marginTop: 5,
            }}
          >
            INICIAR
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CircleButton;
