import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from "react-native";
import React, { useState } from "react";
import {
  DrawerContentScrollView,
  DrawerItemList,
} from "@react-navigation/drawer";
import { useNavigation } from '@react-navigation/core'
import { auth } from "../firebase";

import Ionicons from "react-native-vector-icons/Ionicons";
import { colors } from "./colors";

const CustomDrawer = (props) => {
  const navigation = useNavigation()
  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        console.log("cerrar")
      })
      .catch((error) => alert(error.message));
  };

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{ backgroundColor: colors.night }}
      >
        <ImageBackground
          source={require("../assets/images/background.jpg")}
          style={{ padding: 20 }}
        >
          <Image
            source={require("../assets/images/rick-and-morty.jpg")}
            style={{
              height: 80,
              width: 80,
              borderRadius: 40,
              marginBottom: 10,
            }}
          />
          <Text
            style={{
              color: colors.white,
              fontSize: 18,
              fontFamily: "Roboto",
            }}
          >
            Alvaro Zapata
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                color: colors.white,
                fontFamily: "Roboto",
                marginRight: 5,
              }}
            >
              {auth.currentUser?.email}
            </Text>
            <Ionicons name="person" size={14} color={colors.white} />
          </View>
        </ImageBackground>
        <View
          style={{
            flex: 1,
            backgroundColor: colors.white,
            paddingTop: 10,
          }}
        >
          <DrawerItemList {...props} />
        </View>
      </DrawerContentScrollView>
      <View
        style={{
          padding: 20,
          borderTopWidth: 1,
          borderTopColor: colors.night,
        }}
      >
        <TouchableOpacity
          onPress={() => {}}
          style={{
            paddingVertical: 15,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Ionicons name="share-social-outline" size={22} />
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Roboto",
                marginLeft: 5,
              }}
            >
              Compartir
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleSignOut}
          style={{
            paddingVertical: 15,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Ionicons name="exit-outline" size={22} />
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Roboto",
                marginLeft: 5,
              }}
            >
              Cerrar Sesión
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CustomDrawer;

const styles = StyleSheet.create({});
