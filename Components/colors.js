export const colors = {
    white:'#fff',
    primary:'#001845',
    secondary:'#0466C8',
    tertiary:'#33415C',
    gray:'#5C677D',
    grayLight:'#979DAC',
    grayDark:'#4B5563',
    accent:'#fbcd77',
    night:'#242f3e',
    black: '#000',
    success: '#22bb33',
    danger: '#bb2124',
    warning: '#f0ad4e',
}