import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { Dimensions } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import { colors } from '../colors'

const RideItem = (props) => {
    const image = require('../../assets/images/person.png')
    return (
        <View style={{
            flexDirection:'row',
            justifyContent:'space-between',
            alignItems:'center',
            backgroundColor: colors.tertiary,
            borderRadius: 8,
            borderBottomWidth: 1,
            borderBottomColor: colors.gray,
            }}
        >
            <View
                style={{
                    flexDirection:'row',
                    alignItems:'center',
                    flex:1,
                }}
            >
                <Image
                    source={ image }
                    style={{
                        width:50,
                        height:50,
                        borderRadius:10,
                        marginRight:8,
                        margin: 3,
                    }}
                />
                <View style={{
                    width: Dimensions.get('window').width - 220
                }}>
                    <Text
                        style={{
                            color:colors.white,
                            fontFamily: 'Roboto',
                            fontSize: 14,
                        }}
                    >
                        Cliente 5 <Ionicons name="star" size={15} />
                    </Text>
                    <Text
                        numberOfLines={2}
                        style={{
                            color:colors.white,
                            fontFamily: 'Roboto',
                            fontSize: 12,
                        }}
                    >
                        Av. Juana azurduy N 123
                    </Text>
                </View>
            </View>
            <TouchableOpacity
                style={{
                    backgroundColor: colors.success,
                    padding: 10,
                    width: 80,
                    borderRadius: 10,
                    marginRight: 10,
                }}
            >
                <Text
                    style={{
                        color:colors.white,
                        textAlign: 'center',
                        fontFamily: 'Roboto',
                        fontSize: 14
                    }}
                >Aceptar</Text>
            </TouchableOpacity>
        </View>
      )
}

export default RideItem

const styles = StyleSheet.create({})