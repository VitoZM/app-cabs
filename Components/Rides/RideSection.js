import { StyleSheet, Text, View, Animated, ScrollView } from 'react-native'
import React, { useState } from 'react'
import { ScreenHeight, ScreenWidth } from '../shared'
import Ionicons from "react-native-vector-icons/Ionicons"
import RideItem from './RideItem'

import { colors } from '../colors'

const RideSection = (props) => {

    const [alignment] = useState(new Animated.Value(0))
    const [isUnfolded, setIsUnfolded] = useState(false)
    const [icon, setIcon] = useState("chevron-up")

    const moveActionSheet = () => {
        if(isUnfolded){
            Animated.timing(alignment, {
                toValue: 0,
                duration: 500,
                useNativeDriver: false,
            }).start()
            setIsUnfolded(false)
            setIcon("chevron-up")
        }else{
            Animated.timing(alignment, {
                toValue: 1,
                duration: 500,
                useNativeDriver: false,
            }).start()
            setIsUnfolded(true)
            setIcon("chevron-down")
        }
    }

    const actionSheetInterpolate = alignment.interpolate({
        inputRange: [0, 1],
        outputRange: [-ScreenHeight / 2.4 + 50, 0]
    })

    const actionSheetStyle = {
        bottom: actionSheetInterpolate
    }

    const gestureHandler = (e) => {
        console.log("ASDF")
        if(e.nativeEvent.contentOffset.y > 0)
            bringUpActionSheet()
    }

  return (
    <Animated.View style={[styles.container, actionSheetStyle]}>
        
        <View
            style={{
                width: ScreenWidth ,
                backgroundColor: colors.tertiary,
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
                flexDirection:'row',
                justifyContent:'space-between',
                paddingBottom: 10,
            }}
        >
            <View
                style={{
                        marginHorizontal: 20,
                        marginTop: 8,
                        borderRadius: 5,
                        borderColor: colors.white,
                        borderWidth: 1,
                        padding: 2,
                        paddingHorizontal: 10
                    }}
            >
                <Text
                    onPress={moveActionSheet}
                    style={{
                        fontFamily:"Roboto",
                        fontSize: 20,
                        fontWeight: "bold",
                        color: colors.white
                    }}
                >5</Text>
            </View>
            <Text
                style={{
                    marginTop: 8,
                    fontFamily:"Roboto",
                    fontSize: 20,
                    fontWeight: "bold",
                    color: colors.white,
                }}
            >Estás Conectado</Text>
            <View
                style={{
                    width: "20%",
                    justifyContent: 'center',
                    marginRight: 20,
                }}
            >
                <Ionicons
                    style={{
                        alignSelf: 'flex-end',
                        borderRadius: 5,
                        borderColor: colors.white,
                        borderWidth: 1,
                        marginTop: 5,
                        color: colors.white,
                    }}
                    onPress={moveActionSheet}
                    name={icon}
                    size={32}
                />
            </View>
        </View>
        <ScrollView
            style={{
                margin:10,
                borderRadius: 8,
                backgroundColor: colors.tertiary,
            }}
        >
            <RideItem  />
        </ScrollView>
    </Animated.View>
  )
}

export default RideSection

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.gray,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: ScreenHeight / 2.4,
        width: ScreenWidth,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginHorizontal: 1,
    },
    grabber: {
        width: 60,
        borderTopWidth: 5,
        //marginTop: 10,
        alignSelf: "center"
    },
})