import { StyleSheet, Text, View, Animated, ScrollView } from 'react-native'
import React, { useState } from 'react'
import { ScreenHeight, ScreenWidth } from './shared'
import Ionicons from "react-native-vector-icons/Ionicons"
import ListItem from './ListItem'
import CircleButton from './buttons/CircleButton'

import { colors } from './colors'

const ClientCard = (props) => {

    const [alignment] = useState(new Animated.Value(0))
    const [isUnfolded, setIsUnfolded] = useState(false)
    const [icon, setIcon] = useState("chevron-up")

    const moveActionSheet = () => {
        
        if(isUnfolded){
            Animated.timing(alignment, {
                toValue: 0,
                duration: 500,
                useNativeDriver: false,
            }).start()
            setIsUnfolded(false)
            setIcon("chevron-up")
        }else{
            Animated.timing(alignment, {
                toValue: 1,
                duration: 500,
                useNativeDriver: false,
            }).start()
            setIsUnfolded(true)
            setIcon("chevron-down")
        }
    }

    const actionSheetInterpolate = alignment.interpolate({
        inputRange: [0, 1],
        outputRange: [-ScreenHeight / 2.4 + 50, 0]
    })

    const actionSheetStyle = {
        bottom: actionSheetInterpolate
    }

    const gestureHandler = (e) => {
        console.log("ASDF")
        if(e.nativeEvent.contentOffset.y > 0)
            bringUpActionSheet()
    }

  return (
    <Animated.View style={[styles.container, actionSheetStyle]}>
        
        <View
            style={{
                width: ScreenWidth / 1.05,
                backgroundColor: colors.tertiary,
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
                flexDirection:'row',
                justifyContent:'space-between',
                paddingBottom: 10,
            }}
        >
            
            <Text
                style={{
                    marginTop: 8,
                    marginLeft: 8,
                    fontFamily:"Roboto",
                    fontSize: 20,
                    fontWeight: "bold",
                    color: colors.white,
                }}
            >Llegando por Rigoberto</Text>
            <View
                style={{
                    width: "20%",
                    justifyContent: 'center',
                    marginRight: 20,
                }}
            >
                <Ionicons
                    style={{
                        alignSelf: 'flex-end',
                        borderRadius: 5,
                        borderColor: colors.white,
                        borderWidth: 1,
                        marginTop: 5,
                        color: colors.white,
                        backgroundColor: colors.danger,
                    }}
                    onPress={moveActionSheet}
                    name="ios-close"
                    size={32}
                />
            </View>
        </View>
        <ScrollView
            style={{
                margin:10,
                borderRadius: 8,
            }}
        >
            <ListItem
                image="1"
                which="Inicio"
                address="Av. Juana Azurduy S/N"
            />
            <ListItem
                image="2"
                which="Destino"
                address="Av. Rosedal #135"
            />
            <CircleButton />
        </ScrollView>
    </Animated.View>
  )
}

export default ClientCard

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.gray,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: ScreenHeight / 2.4,
        width: ScreenWidth / 1.05,
        borderRadius: 10,
        margin: 10,
        zIndex: 100,
    },
    grabber: {
        width: 60,
        borderTopWidth: 5,
        //marginTop: 10,
        alignSelf: "center"
    },
})